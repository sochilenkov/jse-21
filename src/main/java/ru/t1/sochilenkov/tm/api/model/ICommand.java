package ru.t1.sochilenkov.tm.api.model;

import ru.t1.sochilenkov.tm.enumerated.Role;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    Role[] getRoles();

    void execute();

}
