package ru.t1.sochilenkov.tm.api.repository;

import ru.t1.sochilenkov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

}
