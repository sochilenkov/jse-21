package ru.t1.sochilenkov.tm.command.user;

import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Update profile of current user.";

    public static final String NAME = "user-update-profile";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("ENTER NEW FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
